# MINI GIT MANUAL 

É importante salientar que antes de começar a usar o GIT(version control), se faz necessário, meio que de certa forma, inicializar e determinar as configurações básicas e padrões do próprio GIT. O motivo dessa recomendação é basicamente para manter as boas práticas como usuário e porque elas lhe serão úteis para identificação ao comunicar-se com um repositório remoto.

`$ config --global --edit` => para definir as configurações globais do GIT no repositório local. 

Minha sugestão de configuração é semelhante à:

    1 [user]
    2     email = <email@exemplo.com>
    3     name = <seu_nome_de_usuário_preferencialmente>
    4 [core]
    5     editor = vim

Minha escolha particular é ter o "VIM" como editor principal; o usuário, contudo, pode acabar escolhendo outro como o "nano", "gedit" etc.

Caso o usuário preferir, há também a possibilidade de definir comando a comando as configurações iniciais básicas acima recomendadas.

`$ git config --global user.name <seu_nome_de_usuário_preferencialmente>` => para definir o nome de usuário de sua escolha.
`$ git config --global user.email <email@exemplo.com>` => para definir o endereço de e-mail de sua escolha.
`$ git config --global core.editor <seu_editor_de_texto>` => para definir o seu editor de texto pelo terminal.

Vale lembrar que os sinais de maior e menor não são escritos, eles estão aqui apenas para indicar que o usuário deve substituir esse pedaço de texto por algo que se refere a descrição que está entre eles. Logo, como um exemplo do último comando, o resultado seria como:

`$ git config --global core.editor vim` => e consequentente definiria que o "vim" seria o editor de texto padrão do GIT.

#### Após já ter o git configurado, é hora de usá-lo.

#### - Lembre da diferença entre os termos "repositório local" e "repositório remoto".

- Repositório local: local em que o usuário escolhe para armazenar, salvar("commitar") seus arquivos seja de algum projeto ou não. O nome local é referente a própria máquina, ao computador do usuário, muitas vezes.

- Repositório remoto: loca em que o usuário escolhe para armazanar... o nome remoto é referente a um local remoto para a hospedagem de seus arquivos, geralmente. Exemplos: Gitlab, Github, Gitea, Bitbucket e outros...

### Iniciando um repositório local:

`$ git init` => inicia um repositório local.

#### Conectando seu repositório local a um repositório remoto. Em outras palavras, adicionando um repositório remoto a seu repositório local:

`$ git remote add <remote> <link>` => liga/adiciona um repositório remoto ao seu repositório local de escolha. Note que o termo `<remote>` é apenas um nome/apelido que o usuário deve dar ao seu repositório remoto. Por configiração padrão ele chama-se "origin". Se o usuário estiver trabalhando com mais de um repositório remoto ao mesmo tempo ele deve trocar esse nome/apelido padrão por, de preferência, o nome do repositório no qual ele estiver trabalhando por questões práticas. 

* Exemplo: 

    - `git remote add origin` `<link>` => como exemplo para caso ele só esteja trabalhando com apenas um repositório.
	- `git remote add gitlab` `<link>` => como exemplo para o gitlab.
	- `git remote add github` `<link>` => como exemplo para o guthub.
	- `git remote add gitea` `<link>` => como exemplo para o gitea.
	- `git remote add bitbucket` `<link>` => como exemplo para o bitbucket.

e assim sucessivamente... lembre-se que o termo `<link>` é o link fornecido pelo seu repositório remoto de escolha.

`$ git remote` => lista por nome/apelido todos os repositórios remotos adicionados/ligados ao seu repositório local.

`$ git remote -v` => lista "verbosamente" todos os repositórios remotos adicionados/ligados ao seu repositório local de maneira completa("verbosa"). Mostra o seu apelido/nome seguido de seu link referente ao repositório remoto adicionado.

#### - Pronto, a parte bruta e básica de conexão entre o repositório local e remoto está feita.

## Trabalhando em um arquivo/projeto através de ramificações(branches).

Sabendo adicionar um repositório remoto ao repositório local, o próximo passo é aprender a trabalhar com segmentos de versão(ramificações/branches) de algum porojeto. Uma branch, a grosso modo, é como se fosse uma ramificação. Ao querer implementar algo novo e experimental em algum projeto, o qual não se sabe se dará certo ou errado, é totalmente aconselhável que o usuário crie uma ramificação - uma linha de projeto à parte da atual - para que então possa-se trabalhar de forma segura no referente projeto/arquivo. Para tal, eis os comandos:

`$ git branch` => lista todas as branches já criadas pelo usuário. Por definção/configuração padrão, a branch inicial chama-se "master".

`$ git branch <new_branch>` => cria uma nova branch; <new_branch> deve ser substituído pelo nome que o usuário quiser dar a sua nova branch.

Exemplo: $ git branch submaster => cria uma nova branch sob o nome de "submaster".

`$ git checkout <branch>` => destaca o usuário(HEAD) de sua branch atual e o desloca para a <branch> escolhida.

Exemplo: $ git branch submaster => desloca o usuário(HEAD) para o segmento de linha/ramificação(branch) "submaster" caso ela exista. Contrariamente uma mensagem de erro é esperada.

Observação: Caso o usuário(HEAD) queira deslocar-se para uma branch que ainda não existe... ao invés de entrar-se com dois comandos:

`$ git branch <new_branch>`

`$ git checkout <new_branch>`

Pode-se simplesmente inserir o seguinte comando:

`$ git checkout -b <new_branch>` => ou seja, o usuário(HEAD) será destacado da branch atual e será deslocado para uma nova instância de segmento de linha ou ramificação(branch) que será criada ao mesmo passo atráves do comando `"-b"` sob o nome que o segue `<new_branch>`.

* Exemplo: 

  - `$ git checkout -b sub_branch` => deslocará o uruário(HEAD) de sua atual branch para a branch recém criada sob o nome de `sub_branch`.

Para o usuário(HEAD) verificar onde está e se está em alguma branch específica:

`$ git branch`

* **Observação**: caso o usuário(HEAD) esteja completamente destacado/deslocado de qualquer uma das branches, aparecerá "(HEAD) DETATCHED <commitID>", ou algo similar, mas que indica que o usuário não está na cabeça/não está junto de nenhuma branch mas sim em um commit com um "id"(número de identificação) específico. *Mais sobre os "ids" de commits à frente.

### Algumas das funcionalidades do Git(version control) para salvar arquivos em um repositório local.

`$ git add .` => adiciona todos os arquivos de uma vez ao "stage" de seu repositório local. O "stage" é um lugar em que se "armazena" temporariamente os arquivos de deseja salvar("commitar") em seu repositório local.

`$ git add <file1> <file2> <file3> ... <fileN>` => adiciona N arquivos específicos à area de "stage".

`$ git commit` => salva("commita") os arquivos em seu repositório local. Após entrar o comando em um terminal, o usuário será automaticamente levado ao seu editor padrão configurado em seu GIT. Lá, ele deverá escrever o motivo do salvamento(commit) a ser realizado.

`$ git commit -m "message"` => versão simplificada(shorthand) do passo anterior.

#### - Pronto, até aqui o usuário já foi capaz de salvar aquivos em seu repositório local. Para verificar os commits realizados em seu repositório local ou ainda para verificar qual a situação("status") na qual certos arquivos se encontram em seu repositório local, o usuário pode digitar os seguintes comandos a qualquer hora entre e após os passos anteriores:

`$ git status` => mostra a situação dos arquivos do usuário, se eles estão ou não na área de "stage" para que possam ser salvos.

`$ git log` => mostra os salvamentos(commits) realizados até o momento da entrada do comando.

`$ git log --graph --full-history --all` => mostra em em formato de árvore todos os salvamentos(commits) realizados pelo usuário até o momento da entrada do comando.

Perceba que ao entrar o comando `$ git log`, cada salvamento(commit) porta consigo/possui um "id" próprio gerado pelo git. Essa identificação (id) serve para que ou você retorne a salvamentos anteriores ou os observe.

`$ git checkout <commitId>` => Para destacar a posição atual do usuário(HEAD) da ramificação(branch) na qual ele próprio(HEAD) está trabalhando e ir para o salvamento(commit) desejado.

#### - Até aqui já foi apresentado ao usuário a parte mais básica de git. Até aqui é o mínimo que é esperado que um usuário de git tenha de conhecimento/saiba para que ele possa sobreviver em qualquer repositório local de modo minimamente satisfatório sem causar grandes problemas.

## Um ou mais passos que ***nessa versão*** de publicação não será(ão) exibida(s), tão pouco detalhada(s) por motivos de praticidade são:
- como o usuário pode retornar a um salvamento(commit) específico desfazendo ou excluindo os o que foi feito depois do salvamento(commit) especificado;
- como desfazer um salvamento(commit) específico;
-como reescrever/editar a mesnagem de um salvamento(commit) já realizado.
- como retornar a um ou mais commits anteriores apagando os commits atuais;
- como limpar o histórico completo do repositório local e remoto;

##### Essas dicas serão publicadas e escritas por mim nas próximas versões de publicação desse texto(paper) após devidas correções e reorganizações de tópicos e/ou incrementos de outras partes básicas de comandos. Caso você esteja ansioso em sabê-las entre em contato mostrando seu interesse para que eu então possa priorizar essa implementação.

##### Agora, sabendo o básico de um repositório local e como utilizá-lo de maneira mínima, cabe ao usuário saber manipular e comunicar-se com o repositório remoto de sua preferência.

##### A parte básica de conexão/ligação/comunicação com seu  repositório remoto ao seu repositório local e vice-versa já foi feita/estabelecida anteriormente nesse manual básico de git. Cabe agora ser instruído como enviar o que está salvado("commitado") no repositório local assim como  manipular aquivos que estão no repositório remoto e/ou como acessá-los de maneira geral e/ou específica.

## Primeiro, como enviar(upload/push) arquivos savados("commitaods") no repositório local ao repositório remoto.

`$ git push <remote> <branch>` => envia(upload/push) o(s) salvamento(s)(commit(s)) realizados até então no repositório local ao repositório remoto(`<remote>`) e apenas sob uma ramifição(`<branch>`) específica.

=> Lembre-se que o `<remote>` é o nome/apelido que o usuário escolhe dar ao seu repositório remoto e que por definição padrão, caso não alterado ele chama-se "origin".

=> Lembre-se que `<branch>` é o nome da linha de segmentação/ramificação(branch) na qual o usuário está trabalhando/escolheu trabalhar e que a branch definida por padrão pelo git(controle de versão/version control) chama-se "master".

`$ git push -u <remote> <branch>` => o usuário define(através do comando -u; versão reduzida(shorthand) de `--set-upstream` qual será agora o repositório remoto(`<remote>`) assim como o ramo(`<branch>`) padrão. Esse comando permite que caso o usuário digite pelas próximas vezes o comando: `$ git push`, os arquivos já salvados("commitados") serão automaticamente enviados(uploaded/pushed) para o repositório remoto(`<remote>`) e para o ramo(`<branch>`) que foi definida.

`$ git push --all` => envia(upload/push) TODAS as ramificações(branches) que estão no repositório local e TODOS os arquivos salvados("commitados") em suas respectivas ramificações(branches).

`$ git push -u --all` => define como padrão o envio(upload/push) de TODAS as ramificações(branches) e TODOS os seus arquivos que foram salvos("commitados"). 
